 ts-app

Transaction Server repo

=======
# What is this?

This is template repository to be used for HCL Commerce v9.1. It will contain all the customization done for the ts-app
container.

It uses a multi-stage docker file that makes use of ts-util and ts-app. It compiles the code using ts-util, and then the output zipfile is copied into ts-app for deployment. The end result is a new ts-app container with custom code applied from this git repository and using the code in <THIS_REPO>/workspace with the build properties set in <THIS_REPO>/


Your code should be checked in inside of /workspace

# How does it work?

1. Check in your code inside of /workspace. Note To ensure that the build process is fast check in only customization. You can also check in all the code but it will make the build process longer and more complicated as there will need to be modification to the build script to ignore all the extra files. We strongly recomend to check in only code that has been added, or files that have been modified.

2. Install docker 
3. run 'docker build ./ -t'

In most cases this asset will require some modification to match your enviroment.

# What does it do?

1. Pull the ts-util image and runs the ts-util container
1. Copy the workspace inside container
1. Run the wcb utility to get this zipBundle
1. Copy this zipBundle outside the container
1. Now build the Dockerfile.
1. Pull the image of application (ts-app)
1. Copy the zipBundle inside the container
1. Run the code to get the customized code deployed.
1. Do the docker build and push the updated image to image registry (ECR,GCR etc)
1. Update the latest build number in helm chart values file
1. Run the helm install to install the latest image in cluster.
