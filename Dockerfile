#
# This is a multistage dockerfile(meaning it has two parts) for building a HCL Commerce container. It makes use of the ts-util container to build
# the code and then copies the bundle and applies it to the container being built
# BUILD_TYPE = 'ts','search','xc'

# ##############
#
# FIRST STAGE  #
#
# ##############
FROM ts-utils:9.1.10 as TS-UTIL

# #######
RUN echo "Step 0 - Define all variables"
# #######

ARG CUSTCODE="cust"
ARG BUILD_TYPE="ts"
ARG BUILD_NUMBER="0"
# To be used on the second build "FROM"
ARG IMAGE_TAG=9.1.11.0
ARG IMAGE_REPO

ENV BUILD_IMAGE="ts-utils"
ENV COMMERCE_HOME="/opt/WebSphere/CommerceServer90"

# #######
RUN echo "Step 1 - Prepare the ts-utils container"
# #######

WORKDIR ${COMMERCE_HOME}/
RUN mkdir -p ${COMMERCE_HOME}/gitrepo/
RUN mkdir -p ${COMMERCE_HOME}/src/${BUILD_TYPE}

# ####
RUN echo "Step 2 - Assume the local directory contains the GIT source"
# ####

COPY ./ ${COMMERCE_HOME}/gitrepo


# ####
RUN echo "Step 3 - Copy custom code for image we are building into the build container:"
# ####

RUN cp -R gitrepo/workspace ${COMMERCE_HOME}/src/${BUILD_TYPE}
RUN cp -R gitrepo/build.sh ${COMMERCE_HOME}/wcbd/
RUN cp gitrepo/wcbd-build-shared-classpath.xml ${COMMERCE_HOME}/wcbd/
RUN cp gitrepo/wcbd-build-ts-definition.properties ${COMMERCE_HOME}/wcbd/
RUN cp gitrepo/build.properties ${COMMERCE_HOME}/wcbd/
RUN cp gitrepo/build.private.properties ${COMMERCE_HOME}/wcbd/

RUN chmod 755 wcbd/build.sh
RUN chmod +x wcbd/build.sh


# ####
RUN echo "Step 4 - Run build of customization package inside of the build container:"
# ####
WORKDIR ${COMMERCE_HOME}/wcbd
RUN ./build.sh ${CUSTCODE} ${BUILD_TYPE} ${BUILD_NUMBER} 


# ##############
#
# SECOND STAGE #
#
# ##############

#FROM ${IMAGE_REPO}/ts-app:${IMAGE_TAG}
FROM ts-app:9.1.11.0
RUN yum install -y nc && yum -y update && yum clean all
HEALTHCHECK --retries=12 --start-period=120s --interval=10s CMD curl --connect-timeout 5 -I -s -k "https://localhost:5443/wcs/resources/health/ping" | awk '{print $2}' || exit 1
COPY docker-build/ts/scripts /SETUP/scripts/
COPY docker-build/ts/bin /SETUP/bin/
RUN chmod 777 /SETUP/bin/vaultConfigure.sh
COPY docker-build/ts/app /SETUP/app/
RUN mkdir -p /tmp/CusDeploy
COPY --from=TS-UTIL /tmp/wcbd-deploy-server-latest.zip /tmp/CusDeploy/wcbd-deploy-server-latest.zip
RUN unzip /tmp/CusDeploy/wcbd-deploy-server-latest.zip -d /SETUP/Cus/
#RUN cp -r /tmp/CusDeploy /SETUP/Cus/
RUN /SETUP/bin/applyCustomization.sh  



