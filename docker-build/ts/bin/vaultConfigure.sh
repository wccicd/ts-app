#!/bin/bash

#-----------------------------------------------------------------
# Licensed Materials - Property of IBM
#
# WebSphere Commerce
#
# (C) Copyright IBM Corp. 2017 All Rights Reserved.
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with
# IBM Corp.
#-----------------------------------------------------------------


# You can add docker environment parametenr "-e CONFIGURE_MODE=Vault" to trigger vaultConfigure.sh. 
#  1. VaultConfigureModel uses Vault as the Configuration Centre to fetch environment related data and do configuration 
#  2. For VaultConfigureModel, each environment data be orgainzed as below path structure
#        ${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/{TARGET_KEY}
#  3. VaultConfigureModel and CommonConfigureModel can not be used at same time
#  4. IF You use VaultConfigureModel to override preconfig, I do suggest you enable the VAULT_CA to use Vault PKI auto issue certification. which can make sure
#     the SubjectAlternativeName in certificatrion for each component can match the domian name we configured.
#  5. All configures in this scritps, just as reference, to tell you what configure are Mandatory change or input, what are option.

source /SETUP/bin/utilities.sh

#++++++++++++++++++++++++++++++++++++ Mandatory Configure ++++++++++++++++++++++++++++++++++++
# If you want to start transaction container, below configure steps are Mandatory for whole container and related configure data must be provided through Docker environment parameters.
# If you want just change parts of them, you can copy realted scritps and make some change, then put them into /SETUP/bin/preConfigure.sh or /SETUP/bin/custConfigure.sh. By this way you don't need to enable this default config model anymore.
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#============================ Mandatory Parameters Check =================================
# Check if mandatory parameters have been provided through Container's environment parameter when container startup, otherwise, configure scritps will exit.
if [[ -z "$TENANT" ]] ; then
  echo "TENANT has no value."; exit 1
fi
if [[ -z "$ENVIRONMENT" ]] ; then
  echo "ENVIRONMENT has no value."; exit 1
fi
if [[ -z "$ENVTYPE" ]] ; then
  echo "ENVTYPE has no value."; exit 1
fi
if [[ -z "$VAULT_TOKEN" ]]; then
  echo "VAULT_TOKEN has no value."; exit 1
fi
if [[ -z "$VAULT_URL" ]]; then
  echo "VAULT_URL has no value."; exit 1  
fi 
# Store web domain needs to be passed in and be used when store preview from transaction server
if [[ -z "$STOREWEB_HOST" ]] ; then
  echo "STOREWEB_HOST has no value."; exit 1
fi

#============================= Set Service Domaian Name ==================================
# Domain Name used to satify different requirement. 
if [[ ! -n "$DOMAIN_NAME" ]]; then
       DOMAIN_NAME=`getParameter "DOMAIN_NAME" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/domainName"`

       if [ "$DOMAIN_NAME" == "null" ] || [ "$DOMAIN_NAME" == "" ];then
          echo "set DOMAIN_NAME as default value."
          DOMAIN_NAME="default.svc.cluster.local"
       else
          echo "set DOMAIN_NAME from Vault."
       fi
else
    echo "set DOMAIN_NAME from OS environment parameter."
fi
echo "use domain name: ${DOMAIN_NAME}."


#============================= Set Merchant key ===============================
# IF you set the Merchant Key on Vault, it will update the exist one, if it can not find it on Vault, it will try to get it from OS env parameters "MERCHANTKEY_ENCRYPT"
merchant_key_encrypte=`getParameter "MERCHANTKEY_ENCRYPT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/merchantKeyEncrypte"`
if [ "${merchant_key_encrypte}" != "null" ]; then
  echo "update-encrypted-merchantkey from vault"
  run update-encrypted-merchantkey ${merchant_key_encrypte}
  if [ $? -ne 0 ] ; then
     echo "update-encrypted-merchantkey <merchant_key_encrypte> by Vault returned error."; exit 1
  fi
fi

#============================= Set SPIUSER and SIPUSER_PWD ===============================
spiuser_name=`getParameter "SPIUSER_NAME" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/spiUserName"`
spisuer_pwd=`getParameter "SPIUSER_PWD" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/spiUserPwd"`
# Update spiuser name and password 
if [ "${spiuser_name}" != "null" ] && [ "${spisuer_pwd}" != "null" ]; then
  echo "set spi user and password."
  run set-spi-user ${spiuser_name} ${spisuer_pwd}
  if [ $? -ne 0 ]; then
    echo "set-spi-user <spiuser>:${spiuser_name} <spipass_encrypt>  by Vault returned error."; exit 1
  fi
fi

#============================= Fetch Enviornment Data  ===================================
# Fetch db information from Vault
DBHOST=`getParameter "DBHOST" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbHost"`
DBNAME=`getParameter "DBNAME" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbName"`
DBPASS=`getParameter "DBPASS" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbPassword"`
DBPORT=`getParameter "DBPORT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbPort"`
DBUSER=`getParameter "DBUSER" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbUser"`
DBTYPE=`getParameter "DBTYPE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbType"`
DBAUSER=`getParameter "DBAUSER" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbaUser"`
DBAPASSENCRYPT=`getParameter "DBAPASSENCRYPT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbaPassEncrypt"`
DBPASSENCRYPT=`getParameter "DBPASSENCRYPT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbPassEncrypt"`
DB_SSLENABLE=`getParameter "DB_SSLENABLE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbSSLEnable"`
DB_XA=`getParameter "DB_XA" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbXA"`

# Check if DB inforamtion is Emtpy. Otherwise, the configure shell scirpts will exist.
if [[ "$DBHOST" = "null"  ]] ; then
  echo "DBHOST has no value."; exit 1
fi
if [[ "$DBNAME" = "null"  ]] ; then
  echo "DBNAME has no value."; exit 1
fi
if [[ "$DBPASS" = "null"  ]] ; then
  echo "DBPASS has no value."; exit 1
fi
if [[ "$DBPORT" = "null"  ]] ; then
  echo "DBPORT has no value."; exit 1
fi
if [[ "$DBUSER" = "null"  ]] ; then
  echo "DBUSER has no value."; exit 1
fi

# Set search service port / search service hostname dependence on the ENV Type / Search hostname should based on the env type to decide which hostname should be used
searchPort=`getParameter "SEARCH_PORT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/searchPort"`
if [[ "$searchPort" = "null"  ]] ; then
  SEARCH_PORT="3738"
else
  SEARCH_PORT=${searchPort}
fi

# Set service of hostname and port for store
storeHost=`getParameter "STORE_HOST" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/storeHost"`
if [[ "$storeHost" = "null"  ]] ; then
  STORE_HOST="${TENANT}${ENVIRONMENT}${ENVTYPE}crs-app.${DOMAIN_NAME}"
else
  STORE_HOST=${storeHost}
fi

storePort=`getParameter "STORE_PORT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/storePort"`
if [[ "$storePort" = "null" ]]; then
  STORE_PORT="8443"
else
  STORE_PORT=${storePort}
fi

# Set port for store previous
storeWebPort=`getParameter "STOREWEB_PORT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/storeWebPort"`
if [[ "$storeWebPort" = "null" ]]; then
  STOREWEB_PORT="443"
else
  STOREWEB_PORT=${storeWebPort}
fi


# Set service of hostname and port for user extension service
xcHost=`getParameter "XC_HOST" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/xcHost"`
if [[ "$xcHost" = "null" ]]; then
   XC_HOST="${TENANT}${ENVIRONMENT}${ENVTYPE}xc-app.${DOMAIN_NAME}"
else
   XC_HOST=$xcHost
fi

xcPort=`getParameter "XC_PORT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/xcPort"`
if [[ "$xcPort" = "null" ]]; then
    XC_PORT="9443"
else
    XC_PORT=${xcPort}
fi


#============================ Transaction Special Config ==================================
# Check instance type (auth or live) and run commands to setup servers
if [ "$ENVTYPE" = "auth" ] ; then

  # IF transaction server is auth, it must connect to search master node which should be deploy on auth env
  # Set service of hostname and port for search
  searchHost=`getParameter "SEARCH_HOST" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/searchMasterHost"`
  if [[ "$searchHost" = "null" ]];then
    SEARCH_HOST="${TENANT}${ENVIRONMENT}${ENVTYPE}search-app-master.${DOMAIN_NAME}"
  else
    SEARCH_HOST=${searchHost}
  fi

  

  run set-search-server $SEARCH_HOST $SEARCH_PORT
  if [ $? -ne 0 ] ; then
    echo "set-search-server <search_host>:$SEARCH_HOST <search_port>:$SEARCH_PORT returned error."; exit 1
  fi

  # Fetch live db information from Vault. Auth Transaction need to config live db for data propagate
  DBHOST_LIVE=`getParameter "DBHOST_LIVE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/live/dbHost"`
  DBNAME_LIVE=`getParameter "DBNAME_LIVE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/live/dbName"`
  DBPASS_LIVE=`getParameter "DBPASS_LIVE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/live/dbPassword"`
  DBPORT_LIVE=`getParameter "DBPORT_LIVE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/live/dbPort"`
  DBUSER_LIVE=`getParameter "DBUSER_LIVE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/live/dbUser"`
  DB_SSLENABLE_LIVE=`getParameter "DB_SSLENABLE_LIVE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/live/dbSSLEnable"`
  DB_XA_LIVE=`getParameter "DBXA_LIVE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/live/dbXA"`

  # Check if live db data is empty on auth transcation server, if the live db inform is empty, scripts will not config it. but it will impact the data propagate function
  if [[ "$DBHOST_LIVE" = "null"  ]] ; then
        echo "DBHOST_LIVE has no value."; 
  fi
  if [[ "$DBNAME_LIVE" = "null"  ]] ; then
        echo "DBNAME_LIVE has no value."
  fi
  if [[ "$DBPASS_LIVE" = "null"  ]] ; then
        echo "DBPASS_LIVE has no value."
  fi
  if [[ "$DBPORT_LIVE" = "null"  ]] ; then
        echo "DBPORT_LIVE has no value."
  fi
  if [[ "$DBUSER_LIVE" = "null"  ]] ; then
        echo "DBUSER_LIVE has no value."
  fi

  if [[ "$DBHOST_LIVE" = "null" ]] || [[ "$DBNAME_LIVE" = "null"  ]] || [[ "$DBPASS_LIVE" = "null"  ]] || [[ "$DBPORT_LIVE" = "null"  ]] || [[ "$DBUSER_LIVE" = "null"  ]] ; then
     echo "ingore set live db datasource. which may impact the propagate data from auth to live"
  else
        if [ "${DB_XA_LIVE}" == "null" ]; then
            DB_XA_LIVE=true
        fi

        if [ "${DB_SSLENABLE_LIVE}" == "null" ]; then
            DB_SSLENABLE_LIVE=false
        fi

        if [ "${DBTYPE}" == "null" ]; then
            DBTYPE=db2
        fi

        echo "add live db datasource."
        run add-datasource jdbc/WCPublishDataSource $DBNAME_LIVE ${DBTYPE} $DBHOST_LIVE $DBPORT_LIVE $DBUSER_LIVE $DBPASS_LIVE ${DB_XA_LIVE} ${DB_SSLENABLE_LIVE}
        if [ $? -ne 0 ] ; then
          echo "add-datasource jdbc/WCPublishDataSource <database>:${DBNAME} <dbtype>:${DBTYPE} <dbHost>:${DBHOST} <dbPort>:${DBPORT} <dbUser>:<dbUser> <dbPass>:<dbPass> <xa>:${DB_XA} <sslConnect>:${DB_SSLENABLE_LIVE} returned error."; exit 1
        fi
  fi
  
elif [ "$ENVTYPE" = "live" ] ; then
  # IF transaction is on live, it must connect to search repeater
  # Set service of hostname and port for search
  searchSlaveHost=`getParameter "SEARCH_HOST" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/searchSlaveHost"`
  if [[ "$searchSlaveHost" = "null" ]];then
    SEARCH_HOST="${TENANT}${ENVIRONMENT}${ENVTYPE}search-app-slave.${DOMAIN_NAME}"
  else
    SEARCH_HOST=${searchSlaveHost}
  fi
  
  searchRepeaterHost=`getParameter "SEARCH_REPEATER_HOST" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/searchRepeaterHost"`
  if [[ "$searchRepeaterHost" = "null" ]];then
    SEARCH_REPEATER_HOST="${TENANT}${ENVIRONMENT}${ENVTYPE}search-app-repeater.${DOMAIN_NAME}"
  else
    SEARCH_REPEATER_HOST=${searchRepeaterHost}
  fi

  echo "set search server."
  run set-search-server $SEARCH_HOST $SEARCH_PORT $SEARCH_REPEATER_HOST $SEARCH_PORT
  if [ $? -ne 0 ] ; then
    echo "set-search-server <search_host>:$SEARCH_HOST <search_port>:$SEARCH_PORT <search_repeater_host>:$SEARCH_REPEATER_HOST <search_port>:$SEARCH_PORT returned error."; exit 1
  fi
else
  echo "ENVTYPE is invalid."; exit 1
fi

#============================= Set Component Service  =======================================
# Set store service hostname 
echo "set store server."
run set-store-server $STORE_HOST $STORE_PORT
if [ $? -ne 0 ] ; then
  echo "set-store-server store_host store_port returned error."; exit 1
fi

# Set store host name in wc-component.xml for store previous
echo "set store in foundation wc-component.xml"
sed -i "s/value=\"store\"/value=\"${STORE_HOST}\"/g" /opt/WebSphere/AppServer/profiles/default/installedApps/localhost/ts.ear/xml/config/com.ibm.commerce.foundation/wc-component.xml
if [ $? -ne 0 ] ; then
  echo "set store in foundation wc-component.xml returned error."; exit 1
fi

# Set store web host name for store previous
echo "set store web server."
run set-store-web-server $STOREWEB_HOST $STOREWEB_PORT
if [ $? -ne 0 ] ; then
  echo "set-store-web-server storeweb_host:$STOREWEB_HOST storeweb_port:$STOREWEB_PORT returned error."; exit 1
fi

# Set user extension component service hostname, if you don't have the user extension component, that's fine. Even we configure it here, but it will never be used.
echo "set xc server."
run set-xc-server $XC_HOST $XC_PORT
if [ $? -ne 0 ] ; then
  echo "set-xc-server <xc_host>:$XC_HOST <xc_port>:$XC_PORT returned error."; exit 1
fi

#============================ Update DataSource ===========================================
# Update datasource. In OOTB, there already have the default datasource be configured. ( instancename: mall  dbtype: db2 dbport:50000 ) 
if [ "${DB_SSLENABLE}" = "null" ]; then
        DB_SSLENABLE=false
fi

if [ "${DB_XA}" = "null" ];then
        DB_XA=false
fi

if [ -z "${DBTYPE}" ] || [ "${DBTYPE}" = "db2" ]; then
  echo "update datasource."
  run update-datasource-db jdbc/WCDataSource $DBNAME $DBHOST $DBPORT ${DB_SSLENABLE}
  if [ $? -ne 0 ] ; then
      echo "update-datasource-db jdbc/WCDataSource <dbname>:$DBNAME <dbhost>:$DBHOST <dbport>:$DBPORT <dbsslEnable>:${DB_SSLENABLE} returned error."; exit 1
  fi

  # Update db user and password for datasource (as default: dbuser/password: wcs/wcs1 )
  echo "update datasource cred."
  run update-datasource-cred jdbc/WCDataSource $DBUSER $DBPASS
  if [ $? -ne 0 ] ; then
     echo "update-datasource-cred jdbc/WCDataSource <dbuser>:$DBUSER <dbpass> returned error."; exit 1
  fi
elif [ "${DBTYPE}" = "oracle" ]; then
        echo "set oracle datasource."
        run add-datasource jdbc/WCDataSource ${DBNAME} ${DBTYPE} ${DBHOST} ${DBPORT} ${DBUSER} ${DBPASS} ${DB_XA} ${DB_SSLENABLE}
        if [ $? -ne 0 ]; then
           echo "add-datasource jdbc/WCDataSource <database>:${DBNAME} <dbtype>:${DBTYPE} <dbHost>:${DBHOST} <dbPort>:${DBPORT} <dbUser>:<dbUser> <dbPass>:<dbPass> <xa>:${DBXA} <sslConnect>:${DB_SSLENABLE} returned error."; exit 1
        fi
else
  echo "DB type ${DBTYPE} is not OOTB support"    
fi



#+++++++++++++++++++++++++++ Option Configure ++++++++++++++++++++++++++++++++++++
# If you want to config more, you can prepare related configure data on Vault. 
#   1. It will not impact the base function of transaction without below config
#   2. Configure scripts will detect if the value be fetch is null (that means you don't have related key-value on Vault). If so, it will ignore the configureation 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#======================================== OIDC Config ======================================
# For emable IBMID
oidc_ClientID=`getParameter "OIDC_CLIENT_ID" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/oidcClientId"`
oidc_ClientSecret=`getParameter "OIDC_CLIENT_SECRET" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/oidcClientSecret"`
blueid_Server=`getParameter "BLUE_ID_SERVER" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/blueIDServer"`
blueid_ProviderHost=`getParameter "BLUE_ID_PROVIDERHOST" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/blueIDProviderHost"`

if [ "$oidc_ClientID" != "null" ] && [ "$oidc_ClientSecret" != "null" ] && [ "$blueid_Server" != "null" ] && [ "$blueid_ProviderHost" != "null" ] ; then
  echo "set-oidc-configuration for integration IBMid"
  run set-oidc-configuration ${blueid_Server} ${oidc_ClientID} ${oidc_ClientSecret} ${blueid_ProviderHost}
  if [ $? -ne 0 ] ; then
       echo "set-oidc-configuration <blue_id_server>:${blueid_Server} <oidc_client_id>:${oidc_ClientID} <oidc_client_client_secret> <blueid_ProviderHost>${blueid_ProviderHost} returned error."; exit 1
  fi
else
  echo "ignore oidc config for IBMid integration."
fi

#======================================== Construct Custom Trace =====================================
traceSpec=`getParameter "TRACE_SPEC" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/traceSpecification/ts-app"`

if [ "$traceSpec" != "null" ]; then
  echo "set-trace-specification for custom."
  # set the trace specification
  run set-trace-specification "$traceSpec"
  if [ $? -ne 0 ] ; then
    echo "set-trace-specification <trace_spec> returned error."; exit 1
  fi
else
  echo "ignore set-trace-specification for custom"
fi

#======================================== Update-WC-Server-DBAUser ===================================
# This will update the DBA user with encrypted password in wc-server.xml

if [ "$DBAUSER" != "null" ] && [ "DBAPASSENCRYPT" != "null" ] && [ "DBPASSENCRYPT" != "null" ]; then
  echo "update-wc-server-datasource."
  run update-wc-server-datasource $DBTYPE $DBHOST $DBPORT $DBNAME $DBAUSER $DBAPASSENCRYPT $DBUSER $DBPASSENCRYPT
  if [ $? -ne 0 ] ; then
       echo "update-wc-server-datasource <dbType>:$DBTYPE <dbHost>:$DBHOST <dbPort>:$DBPORT <dbname>:$DBNAME <dbauser>:$DBAUSER <dbaencryptedpassword> <dbuser>:$DBUSER <dbencryptedpassword> returned error."; exit 1
  fi
else
  echo "ignore update datasource in wc-server.xml."
fi

#======================================== Construct Custom KAFKA =====================================
# Fetch kafka server and zookeeper server from Vault
kafkaTopicPrefix=`getParameter "KAFKA_TOPIC_PREFIX" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/kafkaTopicPrefix"`
if [[ "$kafkaTopicPrefix" = "null" ]];then
   KAFKA_TOPIC_PREFIX=${TENANT}${ENVIRONMENT}${ENVTYPE}
else
   KAFKA_TOPIC_PREFIX=${kafkaTopicPrefix}
fi 

kafkaServers=`getParameter "KAFKA_SERVERS" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/kafkaServers"`
zookeeperServers=`getParameter "ZOOKEEPER_SERVERS" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/zooKeeperServers"`

if [ "$zookeeperServers" != "null" ] && [ "$kafkaServers" != "null" ]; then
  echo "set-kafka-server for custom."
  run set-kafka-server $kafkaServers $KAFKA_TOPIC_PREFIX $zookeeperServers
  if [ $? -ne 0 ] ; then
      echo "set-kafka-server <kafka_servers>:$kafkaServers <kafka_topic_prefix>:$KAFKA_TOPIC_PREFIX <zookeeper_servers>:$zookeeperServers returned error."; exit 1
  fi
else
  echo "ignore kafka server and zookeeper server."
fi

#========================================  ENABLE HEALTH CENTER ======================================
component_name=`bash -c /SETUP/bin/viewlabels | sed "s/commerce\/\(.*\)=.*/\1/g"`
enableHealthCenter=`getParameter "HEALTH_CENTER_ENABLED" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/healthCenterEnable/${component_name}"`

if [ "$enableHealthCenter" != "null" ] && [ "$enableHealthCenter" == "true" ]; then
  echo "enable health center."
  run add-generic-jvmarg -Dcom.ibm.java.diagnostics.healthcenter.headless.output.directory=/profile/logs/healthcenter
  run add-generic-jvmarg -Dcom.ibm.java.diagnostics.healthcenter.headless.run.duration=15
  run add-generic-jvmarg -Dcom.ibm.diagnostics.healthcenter.data.profiling=off
  run add-generic-jvmarg -Dcom.ibm.java.diagnostics.healthcenter.allocation.threshold.low=10000000
  run add-generic-jvmarg -Dcom.ibm.java.diagnostics.healthcenter.stack.trace.depth=20
  run add-generic-jvmarg -Dcom.ibm.java.diagnostics.healthcenter.headless.files.to.keep=0
  run add-generic-jvmarg -Xhealthcenter:level=headless
else
  echo "ignore config health center"
fi

