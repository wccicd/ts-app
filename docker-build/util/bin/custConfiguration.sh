#!/bin/bash

# Exit this script when an error is encountered
set -e
source /SETUP/bin/utilities.sh

isBase=""

if [ -d /opt/WebSphere/AppServer ];then
       APPPATH=/opt/WebSphere/AppServer/profiles/default/
       isBase=true
elif [ -d /opt/WebSphere/Liberty ];then
       APPPATH=/opt/WebSphere/Liberty/
       isBase=false
fi

if [ -z "$ENVIRONMENT" ]; then

        echo "No targetable customizations to execute ..."
else
        find $APPPATH -type f -name "*.targetable.${ENVIRONMENT}${ENVTYPE}.*" -print0 | while read -d $'\0' f
        do
                new=`echo "$f" | sed -e "s/targetable\.${ENVIRONMENT}${ENVTYPE}\.//"`
                echo "applying taregtable file name on $f to $new"
                mv "$f" "$new"
        done

fi

if [ "${SETUP_DATADEPLOY}" == "true" ]; then

        echo "starting data deploy setup $(date)"

        cd /SETUP/app/dataload/deploy
        if [ -n "${CONFIGURE_MODE}" ] && [ "${CONFIGURE_MODE}" == "Vault" ]; then
                DBHOST=`getParameter "DBHOST" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbHost"`
                DBNAME=`getParameter "DBNAME" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbName"`
                DBPASS=`getParameter "DBPASS" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbPassword"`
                DBPORT=`getParameter "DBPORT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbPort"`
                DBUSER=`getParameter "DBUSER" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbUser"`
                DBTYPE=`getParameter "DBTYPE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbType"`
                DBAUSER=`getParameter "DBAUSER" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbaUser"`
                DBAPASSENCRYPT=`getParameter "DBAPASSENCRYPT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbaPassEncrypt"`
                DBPASSENCRYPT=`getParameter "DBPASSENCRYPT" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbPassEncrypt"`
                DB_SSLENABLE=`getParameter "DB_SSLENABLE" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbSSLEnable"`
                DB_XA=`getParameter "DB_XA" "${VAULT_URL}/${TENANT}/${ENVIRONMENT}/${ENVTYPE}/dbXA"`
        fi

        # Check if DB inforamtion is Emtpy. Otherwise, the configure shell scirpts will exist.
        if [[ "$DBHOST" = "null"  ]] ; then
          echo "DBHOST has no value."; exit 1
        fi
        if [[ "$DBNAME" = "null"  ]] ; then
          echo "DBNAME has no value."; exit 1
        fi
        if [[ "$DBPASS" = "null"  ]] ; then
          echo "DBPASS has no value."; exit 1
        fi
        if [[ "$DBPORT" = "null"  ]] ; then
          echo "DBPORT has no value."; exit 1
        fi
        if [[ "$DBUSER" = "null"  ]] ; then
          echo "DBUSER has no value."; exit 1
        fi

        echo "
        db.user.name=${DBUSER}
        db.user.password=${DBPASS}
        " >>  deploy-${ENVIRONMENT}${ENVTYPE}.private.properties
        echo "
        db.host=${DBHOST}
        db.name=$DBNAME
        db.port=${DBPORT}
        db.schema.name=${DBUSER}
        db.type=${DBTYPE}
        " >> deploy-${ENVIRONMENT}${ENVTYPE}.properties
        if [ -z "${DBTYPE}" ] || [ "${DBTYPE}" == "db2" ]; then
                echo '
                jdbc.driver.path=/SETUP/driver/db2/db2jcc4.jar
                jdbc.driver=com.ibm.db2.jcc.DB2Driver
                jdbc.url=jdbc:db2://${db.host}:${db.port}/${db.name}
                ' >> deploy-${ENVIRONMENT}${ENVTYPE}.properties
                echo 'export CLASSPATH=/SETUP/driver/db2/db2jcc4.jar:${CLASSPATH}' >> /opt/WebSphere/CommerceServer90/wcbd/setenv
        elif [ "${DBTYPE}" == "oracle" ]; then
                echo '
                jdbc.driver.path=/SETUP/driver/oracle/ojdbc8.jar
                jdbc.driver=oracle.jdbc.OracleDriver
                jdbc.url=jdbc:oracle:thin:@//${db.host}:${db.port}/${db.name}
                ' >> deploy-${ENVIRONMENT}${ENVTYPE}.properties
                echo 'export CLASSPATH=/SETUP/driver/oracle/ojdbc8.jar:${CLASSPATH}' >> /opt/WebSphere/CommerceServer90/wcbd/setenv
        else
                echo "DB type ${DBTYPE} is not OOTB support"
        fi
        cp -r /SETUP/app/dataload/deploy/* /opt/WebSphere/CommerceServer90/wcbd
        echo "Data Deploy was setup correctly"
        #./wcbd-ant -buildfile wcbd-deploy.xml -Dtarget.env=${ENVIRONMENT}${ENVTYPE} -Dbuild.label=deploy.data
        #cd -
else
        echo "SETUP_DATADEPLOY is set to ${SETUP_DATADEPLOY}"
fi

