#!/bin/bash
#-----------------------------------------------------------------
# Licensed Materials - Property of IBM
#
# WebSphere Commerce
#
# (C) Copyright IBM Corp. 2017 All Rights Reserved.
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with
# IBM Corp.
#-----------------------------------------------------------------

WORKDIR=/tmp/cert-working
mkdir ${WORKDIR}

DEST=/profile/config/cells/localhost/nodes/localhost

#Check if there have local certs under path /SETUP/certs/custom
if [ -d "/SETUP/certs/custom" ];then

    #iterate each file and import as new certification
    for item in `ls "/SETUP/certs/custom"`
    do
             #just handle json file
             if [ -f "/SETUP/certs/custom/$item" ];then
                   certname=${item%.*}
                   
                   #===================Get Customized KEYSTORE=================================
                   KEYSTORE_PASS=`cat /SETUP/certs/custom/${item} | jq -r ".keystorepass"`
                   #echo "..............${KEYSTORE_PASS}"

                   if [ "${KEYSTORE_PASS}" == "" ] || [ "${KEYSTORE_PASS}" == "null" ];then
                              KEYSTORE_PASS="ibmkey"
                   fi
                   #echo " Keystore password is ${KEYSTORE_PASS} "
           
               
                   #====================Get Certification==================================
                   CERTIFICATE=`cat /SETUP/certs/custom/${item} | jq -r ".certificate"`
                   #echo "..............${CERTIFICATE}"

                   if [ "${CERTIFICATE}" == "" ] || [ "${CERTIFICATE}" == "null" ];then
                         echo " No detect certificate for third party cert ${certname}"
                   else

                   #===============Import Certification For KeyStore=======================
                      PRIVATE_KEY=`cat /SETUP/certs/custom/${item} | jq -r ".private_key"`
                      #echo "..............${PRIVATE_KEY}"
                      echo "${CERTIFICATE}" >  ${WORKDIR}/${certname}-server.pem
                      echo "${PRIVATE_KEY}" >> ${WORKDIR}/${certname}-server.pem

                      echo "run command: openssl pkcs12 -export -in ${WORKDIR}/${certname}-server.pem -out ${WORKDIR}/${certname}-key.p12 -name ${certname} -passout pass:${KEYSTORE_PASS}"
                      openssl pkcs12 -export -in ${WORKDIR}/${certname}-server.pem -out ${WORKDIR}/${certname}-key.p12 -name ${certname} -passout pass:${KEYSTORE_PASS} -passin pass:${KEYSTORE_PASS}

                      echo "run command: keytool -importkeystore -srckeystore ${WORKDIR}/${certname}-key.p12 -srcstorepass ${KEYSTORE_PASS} -srcstoretype PKCS12 -destkeystore ${DEST}/key.jks -deststorepass ${KEYSTORE_PASS} -deststoretype JKS -noprompt"
                      keytool -importkeystore -srckeystore ${WORKDIR}/${certname}-key.p12 -srcstorepass ${KEYSTORE_PASS} -srcstoretype PKCS12 -destkeystore ${DEST}/key.p12 -deststorepass ibmkey -deststoretype PKCS12 -destkeypass ibmkey -noprompt
                      echo " Add new third party certification: ${certname} was assigned to keystore"  

                      #===================Create Dynamic SSL===================
                      DESTINATION_HOST=`cat /SETUP/certs/custom/${item} | jq -r ".destination_host"`
                      if [ "${DESTINATION_HOST}" == "" ];then
                          echo " No detect destination_host for third party cert ${certname}"
                      else
                          echo "create-dynamic-ssl-config ${DESTINATION_HOST} ${certname}"
                          run create-dynamic-ssl-config ${DESTINATION_HOST} ${certname}
                          if [ $? -ne 0 ] ; then
                                  echo "create-dynamic-ssl-config ${DESTINATION_HOST} ${certname} returned error."; exit 1
                          fi
                      fi   

                   fi
                   #===================Import Certification For TrustStore===================
                   ISSUING_CA=`cat /SETUP/certs/custom/${item} | jq -r ".issuing_ca"`
                   if [ "${ISSUING_CA}" == "" ];then
                        echo " No detect issue ca for third party cert ${certname}"
                   else
                        echo "${ISSUING_CA}" > ${WORKDIR}/${certname}-ca.pem
                        echo "import trust CA from ${certname} to trust key store .....!"
                        keytool -importcert -file ${WORKDIR}/${certname}-ca.pem -keystore ${DEST}/trust.p12 -storepass ibmkey -storetype PKCS12 -noprompt -alias ${certname}
                        echo "A new third party certificate: ${certname} was assigned to truststore" 
                   fi   

             fi
    done

else
    echo "Don't find local certification need to import"
fi

rm -rf ${WORKDIR}
rm -rf /SETUP/certs/custom
