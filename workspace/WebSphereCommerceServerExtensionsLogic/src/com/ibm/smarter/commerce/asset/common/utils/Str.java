// -------------------------------------------------------------------
//  IBM Confidential
//  OCO Source Materials
// 
//  WebSphere Commerce
// 
//  (c) Copyright International Business Machines Corporation.
//      1996, 2012
//      All rights reserved.
// 
//  The source code for this program is not published or otherwise
//  divested of its trade secrets, irrespective of what has been
//  deposited with the US Copyright Office.
// -------------------------------------------------------------------

package com.ibm.smarter.commerce.asset.common.utils;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * String Utility class
 * 
 * @author IBM
 * @since Mar 3, 2011
 */
public class Str {

	public static final String BLANK = " ";
	public static final String EMPTY_STRING = "";
	public static final String ENDLINE = System.getProperty("line.separator");
	public static final String F = "F";
	public static final String FALSE = "false";
	public static final String N = "N";
	public static final String NO = "NO";
	public static final String NULL = "null";
	public static final String ONE = "1";
	public static final String T = "T";
	public static final String TRUE = "true";
	public static final String TWO = "2";
	public static final String X = "X";
	public static final String Y = "Y";
	public static final String YES = "YES";
	public static final String ZERO = "0";

	/**
	 * Takes in a String Array and converts to a String separated by delimeter.
	 * 
	 * @param array
	 * @param delim
	 * @return
	 */
	public final static String convertArrayToString(String[] array, String delim) {
		StringBuilder sb = new StringBuilder();

		if (array == null) {
			return null;
		}

		for (String element : array) {
			sb.append(element);
			sb.append(delim);
		}
		return sb.toString();
	}

	/**
	 * Takes a string and a delimiter and for each breaks it up into a STRING array
	 * 
	 * @param array
	 * @param delim
	 * @return
	 */
	public final static String[] convertStringToArray(String str, String delim) {
		ArrayList al = new ArrayList();

		if (str == null) {
			return null;
		}

		String substr = str;
		String arrayElem = null;
		int dlen = delim.length();

		for (int i = str.indexOf(delim); i != -1; i = substr.indexOf(delim)) {
			arrayElem = substr.substring(0, i);
			substr = substr.substring(i + dlen, substr.length());
			al.add(arrayElem);
		}

		String[] sarray = new String[al.size()];
		al.toArray(sarray);

		return sarray;
	}

	/**
	 * @param str
	 * @return
	 */
	public static String deAccent(String str) {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	/**
	 * 
	 * This function trims both strings to see if they are both equal to each other but ignoring case
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public final static boolean equalsIgnoreCaseWTrim(String left, String right) {
		boolean equality = false;

		if (left == null && right == null) {
			equality = true;
		} else if (left == null) {
			equality = false;
		} else if (right == null) {
			equality = false;
		} else {
			// Both are not null
			equality = left.trim().equalsIgnoreCase(right.trim());
		}

		return equality;
	}

	/**
	 * This function trims both strings to see if they are both equal to each other
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public final static boolean equalsWTrim(String left, String right) {
		boolean equality = false;

		if (left == null && right == null) {
			equality = true;
		} else if (left == null) {
			equality = false;
		} else if (right == null) {
			equality = false;
		} else {
			// Both are not null
			equality = left.trim().equals(right.trim());
		}

		return equality;
	}

	/**
	 * A function to create escaped characters for XML
	 * 
	 * @param str
	 * @return
	 */
	public final static String escapeXml(String str) {
		StringBuilder sb = new StringBuilder();

		if (str == null) {
			return EMPTY_STRING;
		} else {
			for (int i = 0; i < str.length(); i++) {
				// &'"<>
				// &amp;&apos;&quot;&lt;&gt;
				if ('&' == str.charAt(i)) {
					sb.append("&amp;");
				} else if ('\'' == str.charAt(i)) {
					sb.append("&apos;");
				} else if ('\"' == str.charAt(i)) {
					sb.append("&quot;");
				} else if ('<' == str.charAt(i)) {
					sb.append("&lt;");
				} else if ('>' == str.charAt(i)) {
					sb.append("&gt;");
				} else {
					sb.append(str.charAt(i));
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Checks if a String is empty (""), null or whitespace only.
	 * <ul>
	 * <li>Str.isBlank(null) = true</li>
	 * <li>Str.isBlank("") = true</li>
	 * <li>Str.isBlank(" ") = true</li>
	 * <li>Str.isBlank("bob") = false</li>
	 * <li>Str.isBlank("  bob  ") = false</li>
	 * </ul>
	 * 
	 * @param str
	 * @return
	 */
	public final static boolean isBlank(String str) {
		if (str == null || str.trim().isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Used to check if an array is empty.
	 * 
	 * @param str
	 * @return
	 */
	public final static boolean isEmpty(String[] str) {
		if (str == null || str.length <= 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Used to check if an string is empty.
	 * 
	 * @param s
	 * @return
	 */
	public final static boolean isEmptyWithTrim(String s) {
		if (s == null) {
			return true;
		} else {
			if (s.trim().length() == 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * Checks many types of false values "F, NO, N, 0, and false" to see if a string means false.
	 * 
	 * NOTE: IMPORTANT if STR is null then str is NOT FALSE, so we return false!
	 * 
	 * <ul>
	 * <li>Str.isFalse(null) = false</li>
	 * <li>Str.isFalse("") = false</li>
	 * <li>Str.isFalse(" ") = false</li>
	 * <li>Str.isFalse("Y") = false</li>
	 * <li>Str.isFalse(" F ") = true</li>
	 * <li>Str.isFalse(" FaLSe ") = true</li>
	 * <li>Str.isFalse(" No ") = true</li>
	 * <li>Str.isFalse(" n ") = true</li>
	 * <li>Str.isFalse(" 0 ") = true</li>
	 * </ul>
	 * 
	 * @param str
	 * @return
	 */
	public final static boolean isFalse(String str) {
		boolean falsehood = false;
		if (str == null) {
			falsehood = false;
		} else {
			str = str.trim();

			if (N.equalsIgnoreCase(str) || NO.equalsIgnoreCase(str) || F.equalsIgnoreCase(str)
					|| FALSE.equalsIgnoreCase(str) || ZERO.equalsIgnoreCase(str)) {
				falsehood = true;
			} else {
				falsehood = false;
			}
		}
		return falsehood;
	}

	/**
	 * Checks if a String is not empty (""), not null and not whitespace only.
	 * 
	 * <ul>
	 * <li>Str.isNotBlank(null) = false</li>
	 * <li>Str.isNotBlank("") = false</li>
	 * <li>Str.isNotBlank(" ") = false</li>
	 * <li>Str.isNotBlank("bob") = true</li>
	 * <li>Str.isNotBlank("  bob  ") = true</li>
	 * </ul>
	 * 
	 * @param str
	 * @return
	 */
	public final static boolean isNotBlank(String str) {

		if (str == null || str.trim().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Returns if is a number.
	 * 
	 * @param s
	 * @return
	 */
	public final static boolean isNumber(String s) {
		if (s == null) {
			return false;
		} else {
			String chars = s.trim();
			for (int i = 0; i < chars.length(); i++) {
				if (Character.isDigit(chars.charAt(i)) == false) {
					return false;
				}
			}
			return true;
		}
	}

	/**
	 * Checks many types of truth values "X, T, YES, Y, 1, and true" to see if a string means true.
	 * 
	 * <ul>
	 * <li>Str.isTrue("F") = false</li>
	 * <li>Str.isTrue("X") = true</li>
	 * <li>Str.isTrue(" T ") = true</li>
	 * <li>Str.isTrue(" True ") = true</li>
	 * <li>Str.isTrue(" Y ") = true</li>
	 * <li>Str.isTrue(" 1 ") = true</li>
	 * </ul>
	 * 
	 * @param str
	 * @return
	 */
	public final static boolean isTrue(String str) {
		boolean truth = false;
		if (str == null) {
			truth = false;
		} else {
			str = str.trim();

			if (Y.equalsIgnoreCase(str) || YES.equalsIgnoreCase(str) || T.equalsIgnoreCase(str)
					|| TRUE.equalsIgnoreCase(str) || ONE.equalsIgnoreCase(str) || X.equalsIgnoreCase(str)) {
				truth = true;
			} else {
				truth = false;
			}
		}
		return truth;
	}

	/**
	 * Returns a zero filled string of the specified length. If the size of the integer is > the requested length, the
	 * left most digits will be truncated.
	 */
	public static String padLeftZeros(Long v, int n) {
		String format = "%0" + n + "d";
		return String.format(format, v);
	}

	/**
	 * Returns a zero filled string of the specified length. If the size of the integer is > the requested length, the
	 * left most digits will be truncated.
	 */
	public static String padLeftZeros(String s, int n) {
		if (s.isEmpty()) {
			return String.format("%1$0" + n + "d", Long.valueOf(0)); // "space fill"
		} else if (s.length() < n) {
			return String.format("%1$0" + n + "d", Long.valueOf(s)); // "zero fill"
		} else {
			return s.substring(s.length() - n);// "truncate left most"
		}
	}

	/**
	 * Removes leading zeros
	 * 
	 * @param str
	 * @return
	 */
	public static String removeLeadingZeros(String str) {
		if (str == null) {
			return null;
		}
		char[] chars = str.toCharArray();
		int index = 0;
		for (; index < str.length(); index++) {
			if (chars[index] != '0') {
				break;
			}
		}
		return (index == 0) ? str : str.substring(index);
	}

	/**
	 * Returns an upper of the String OR NULL.
	 * 
	 * @param str
	 * @return
	 */
	public static String toUpper(String str) {
		return (str == null) ? null : str.toUpperCase();
	}

	/**
	 * Returns an upper of the String w/ Trim OR NULL.
	 * 
	 * @param str
	 * @return
	 */
	public static String toUpperWTrim(String str) {
		return (str == null) ? Str.EMPTY_STRING : str.toUpperCase().trim();
	}

	/**
	 * Truncate a string by so many characters.
	 * 
	 * @param text
	 * @param i
	 * @return
	 */
	public static String truncate(String text, int i) {
		if (text == null) {
			return null;
		} else if (text.length() <= i) {
			return text;
		} else {
			return text.substring(0, i);
		}
	}

	/**
	 * Returns the string representation of the <code>Object</code> argument.
	 * 
	 * @param obj
	 *            an <code>Object</code>.
	 * @param def
	 *            is default
	 * @return if the argument is <code>null</code>, then default is returned.
	 * @see java.lang.Object#toString()
	 */
	public static String valueOf(Object obj, String def) {
		return (obj == null) ? def : obj.toString().trim();
	}

	/**
	 * Returns the string representation of the <code>Object</code> argument.
	 * 
	 * @param obj
	 *            an <code>Object</code>.
	 * @return if the argument is <code>null</code>, then "" is returned.
	 */
	public static String valueOrEmpty(Object obj) {
		return (obj == null) ? EMPTY_STRING : obj.toString().trim();
	}

	/**
	 * Returns the string representation of the <code>Object</code> argument.
	 * 
	 * @param obj
	 *            an <code>Object</code>.
	 * @return if the argument is <code>null</code>, then <code>null</code> is returned.
	 */
	public static String valueOrNull(Object obj) {
		return (obj == null) ? null : obj.toString().trim();
	}
}
