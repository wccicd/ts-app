# Folder guidelines
 
Following the data structure definde for vault/consul, we apply the same folder methodology here where folders will be called: environmentName/environmentType/targetKey.

- EnvironmentName apply Non-production enviroments: DEV, QA, PREPROD
- EnvironmentType applies to the type of environment: Auth, Live, Common, Shared (Files inside of common will be applied to both Auth and Live)
